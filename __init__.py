# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import ir
from . import statement


def register():
    Pool.register(
        ir.Cron,
        statement.Statement,
        module='account_statement_cron', type_='model')
