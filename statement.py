# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from datetime import datetime
from trytond.model import fields, ModelView, Workflow
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.exceptions import UserError


class Statement(metaclass=PoolMeta):
    __name__ = 'account.statement'

    schedule_at = fields.DateTime('Schedule at', readonly=True)
    schedule_error = fields.Boolean('Schedule error', readonly=True,
        states={
            'invisible': (Eval('state') != 'validating')
        }, depends=['state'])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.state.selection.append(('validating', 'Validating'))
        cls._transitions |= set((
                ('draft', 'validating'),
                ('validating', 'draft'),
                ('validating', 'validated'),
                ))
        for button_name in {'draft', 'validate_statement', 'cancel'}:
            cls._buttons[button_name]['invisible'] &= (
                Eval('state') != 'validating')
        cls._buttons['reconcile']['invisible'] |= (
            Eval('state') != 'validating')
        cls._buttons.update({
            'schedule_validate': {
                'invisible': (Eval('state') != 'draft'),
                'depends': ['state']
            }
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('validating')
    def schedule_validate(cls, statements):
        to_save = []
        for statement in statements:
            # call validate
            getattr(statement, 'validate_%s' % statement.validation)()
            if not statement.schedule_at:
                statement.schedule_at = datetime.now()
            statement.schedule_error = False
            to_save.append(statement)
        if to_save:
            cls.save(to_save)

    @classmethod
    def _get_validate_cron_statements(cls, skip_ids=[]):
        domain = [
            ('state', '=', 'validating'),
            ('schedule_error', '=', False)
        ]
        if skip_ids:
            domain.append(('id', 'not in', skip_ids))
        return cls.search(domain,
            order=[
                ('date', 'ASC'),
                ('schedule_at', 'ASC'),
                ('journal', 'ASC'),
            ], limit=1)

    @classmethod
    def validate_cron(cls):
        statements = cls._get_validate_cron_statements()
        if not statements:
            return

        for statement in statements:
            try:
                cls.validate_statement([statement])
                Transaction().commit()
            except UserError:
                sql_table = cls.__table__()
                with Transaction().new_transaction() as transaction:
                    cursor = transaction.connection.cursor()
                    cursor.execute(*sql_table.update([
                        sql_table.schedule_error], [True],
                        where=(sql_table.id == statement.id)))
                    transaction.commit()
                raise
